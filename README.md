# Gif search app

Aim of this app is to learn and practice new stuff.

## Task description

Create an application from a scratch with possibility to search GIFs through giphy service. 

1. Implement "live search" - i.e. request is sent in N milliseconds (for example 200) after the user has entered some input. 
2. Results are displayed in RecyclerView
3. Request pagination - load enough items to populate the RecyclerView and load more items every time user scrolls to the end of the list (limit / offset)

UI can be very simplistic, but should be responsive and precise, accurate and done according to guidelines. 
Usage of kotlin, rxjava, android databinding library, dagger, mvvm (android architecture components) will be considered as an advantage. 

[Giphy documentation](https://github.com/Giphy/GiphyAPI#search-endpoint)

## Current status and TODOs

NOTES:

1. For images was used Fresco, but due to not supporting Android P, lib were changed to Picasso.
2. DataBinding is not yet implemented.
3. Ideally SearchBar should be moved into toolbar.
4. Because of the decision to use darker theme, search bar may look not ideal. Some custom assets and tinkering with colors should make it better.
