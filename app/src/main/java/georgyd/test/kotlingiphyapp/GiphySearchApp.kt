package georgyd.test.kotlingiphyapp

import android.app.Application
import com.facebook.drawee.backends.pipeline.Fresco
import georgyd.test.kotlingiphyapp.di.AppComponent
import georgyd.test.kotlingiphyapp.di.AppModule
import georgyd.test.kotlingiphyapp.di.DaggerAppComponent
import georgyd.test.kotlingiphyapp.di.NetworkModule

class GiphySearchApp : Application() {

    companion object {
        lateinit var appComponent: AppComponent
    }

    override fun onCreate() {
        super.onCreate()
        initializeDagger()
//        Fresco.initialize(this)
    }

    private fun initializeDagger() {
        appComponent = DaggerAppComponent.builder()
                .appModule(AppModule(this))
                .networkModule(NetworkModule(this))
                .build()
    }

}