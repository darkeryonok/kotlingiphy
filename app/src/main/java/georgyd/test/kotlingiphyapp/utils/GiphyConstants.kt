package georgyd.test.kotlingiphyapp.utils

object GiphyConstants {

    const val API_GIPHY_URL = "https://api.giphy.com/"
    const val API_KEY = "k1XiQUJyEy0pBVne8SGFAQdYs8cc2oWM"
    const val DEFAULT_LIMIT_AND_OFFSET = 20

    const val PARAMETER_KEY_API_KEY = "api_key"
    const val PARAMETER_KEY_LIMIT = "limit"
    const val PARAMETER_KEY_OFFSET = "offset"
    const val PARAMETER_KEY_QUERY = "q"

}