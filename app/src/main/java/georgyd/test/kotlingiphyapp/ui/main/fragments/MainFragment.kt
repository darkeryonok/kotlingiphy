package georgyd.test.kotlingiphyapp.ui.main.fragments

import android.annotation.SuppressLint
import android.content.res.Configuration.ORIENTATION_LANDSCAPE
import android.content.res.Configuration.ORIENTATION_PORTRAIT
import androidx.lifecycle.ViewModelProviders
import android.os.Bundle
import android.util.DisplayMetrics
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import georgyd.test.kotlingiphyapp.R
import georgyd.test.kotlingiphyapp.data.models.ImageWithData
import georgyd.test.kotlingiphyapp.ui.main.MainViewModel
import georgyd.test.kotlingiphyapp.ui.main.adapters.GifAdapter
import georgyd.test.kotlingiphyapp.ui.main.observables.RecyclerScrollObservable
import georgyd.test.kotlingiphyapp.ui.main.observables.SearchViewObservable
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.main_fragment.*
import java.util.concurrent.TimeUnit

class MainFragment : Fragment() {

    private lateinit var adapter: GifAdapter

    companion object {
        fun newInstance() = MainFragment()
    }

    private lateinit var viewModel: MainViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View {
        return inflater.inflate(R.layout.main_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(this).get(MainViewModel::class.java)

        observeViewModel()

        instantiateRecyclerView()
        instantiateSearchBar()
    }

    private fun instantiateRecyclerView() {

        val columns: Int = when (resources.configuration.orientation) {
            ORIENTATION_LANDSCAPE -> {
                4
            }
            ORIENTATION_PORTRAIT -> {
                2
            }
            else -> {
                2
            }
        }

        val displayMetrics = DisplayMetrics()
        activity?.windowManager?.defaultDisplay?.getMetrics(displayMetrics)
        val measuredWidthForElement = Math.round(
                ((displayMetrics.widthPixels - resources.getDimensionPixelSize(R.dimen.activity_recycler_view_margin) * 2) / columns).toFloat())

        gifRecyclerView.layoutManager = GridLayoutManager(context, columns)
        adapter = GifAdapter(measuredWidthForElement)
        gifRecyclerView.adapter = adapter

        setupScrollObservable(gifRecyclerView, adapter)

    }

    private fun instantiateSearchBar() {
        searchBar.isSubmitButtonEnabled = false
        searchBar.setOnCloseListener {
            viewModel.clearImageList()
            adapter.clearData()
            false
        }

        setUpSearchObservable()
    }

    @SuppressLint("CheckResult")
    private fun setUpSearchObservable() {
        SearchViewObservable.fromView(searchBar)
                .debounce(500, TimeUnit.MILLISECONDS)
                .filter { text ->
                    viewModel.updateQuery(text)
                    if (text.isEmpty()) {
                        searchBar.setQuery("", false)
                        viewModel.clearOffset()
                        viewModel.clearImageList()
                        return@filter false
                    } else {
                        return@filter true
                    }
                }
                .switchMap { query -> Observable.just(viewModel.requestDataFromBE(0, query)) }
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe {
                    Toast.makeText(context, "Some gifs loaded.", Toast.LENGTH_SHORT).show()
                }
    }

    @SuppressLint("CheckResult")
    private fun setupScrollObservable(recycler: RecyclerView, adapter: GifAdapter) {
        RecyclerScrollObservable.fromData(recycler, adapter)
                .distinctUntilChanged()
                .filter {
                    if (viewModel.getDataToObserve().value?.size != 0) {
                        viewModel.increaseOffset()
                        return@filter true
                    } else {
                        viewModel.clearOffset()
                        return@filter false
                    }
                }
                .switchMap {
                    return@switchMap Observable.just(viewModel.requestDataFromBE())
                }
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe{
                    Toast.makeText(context, "More gifs loaded.", Toast.LENGTH_SHORT).show()
                }
    }

    private fun observeViewModel() {


        val observer = Observer<MutableList<ImageWithData>> {
            it?.let {
                adapter.setImageList(it)
                if (it.size != 0) {
                    emptyView.visibility = View.GONE
                    gifRecyclerView.visibility = View.VISIBLE
                } else {
                    emptyView.visibility = View.VISIBLE
                    gifRecyclerView.visibility = View.GONE
                }
            }
        }

        viewModel.getDataToObserve().observe(this, observer)
    }

}
