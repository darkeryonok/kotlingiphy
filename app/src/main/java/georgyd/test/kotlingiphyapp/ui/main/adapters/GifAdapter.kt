package georgyd.test.kotlingiphyapp.ui.main.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import georgyd.test.kotlingiphyapp.R
import georgyd.test.kotlingiphyapp.data.models.ImageWithData
import java.util.ArrayList
import com.squareup.picasso.Picasso

class GifAdapter(private val elementWidth: Int) : RecyclerView.Adapter<GifAdapter.ViewHolder>() {

    private var images: MutableList<ImageWithData>

    init {
        images = ArrayList()
    }

    override fun getItemCount(): Int {
        return images.size
    }

    fun setImageList(images: MutableList<ImageWithData>) {
        if (this.images.size == 0) {
            this.images = images
            notifyItemRangeInserted(0, images.size)
        } else {
            val result = DiffUtil.calculateDiff(object : DiffUtil.Callback() {
                override fun getOldListSize(): Int {
                    return this@GifAdapter.images.size
                }

                override fun getNewListSize(): Int {
                    return images.size
                }

                override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
                    return this@GifAdapter.images[oldItemPosition].id === images[newItemPosition].id
                }

                override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
                    val currentImage = images[newItemPosition]
                    val oldImage = images[oldItemPosition]
                    return (currentImage.url == oldImage.url
                            && currentImage.title == oldImage.title
                            && currentImage.url == oldImage.url)
                }
            })
            this.images = images
            notifyDataSetChanged()
            result.dispatchUpdatesTo(this)
        }
    }

    fun addImages(providedImages: MutableList<ImageWithData>) {
        if (images.size > 0) {
            images.addAll(providedImages)
            notifyDataSetChanged()
        } else {
            setImageList(providedImages)
        }
    }

    fun clearData() {
        images.clear()
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(
            parent: ViewGroup,
            viewType: Int): ViewHolder {
        val itemView = LayoutInflater.from(
                parent.context).inflate(R.layout.item_main_image, parent, false)
        return ViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        //fresco fails on Android P, needs update from lib provider
//        holder.simpleDraweeView.setImageURI(images!![position].url)
//        holder.simpleDraweeView.layoutParams.height = elementWidth
//        holder.simpleDraweeView.layoutParams.width = elementWidth

        //for now, Picasso will be alternative
        Picasso.get()
                .load(images[position].url)
                .resize(elementWidth, elementWidth)
                .placeholder(R.mipmap.ic_launcher_round)
                .error(R.mipmap.ic_launcher_round)
                .into(holder.imageView)
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        //fresco fails on Android P, needs update from lib provider
//        internal val simpleDraweeView: SimpleDraweeView = itemView.findViewById(R.id.gif_image) as SimpleDraweeView
        internal val imageView: ImageView = itemView.findViewById(R.id.gif_image) as ImageView
    }
}
