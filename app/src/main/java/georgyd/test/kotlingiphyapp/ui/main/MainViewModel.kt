package georgyd.test.kotlingiphyapp.ui.main

import android.util.Log
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import georgyd.test.kotlingiphyapp.data.api.GiphyApiRepository
import georgyd.test.kotlingiphyapp.data.models.ImageWithData
import georgyd.test.kotlingiphyapp.utils.GiphyConstants
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers

class MainViewModel : ViewModel(), LifecycleObserver {

    private var imagesWithData: MutableLiveData<MutableList<ImageWithData>> = MutableLiveData()

    private var lastQuery: String = ""
    private var lastOffset: Int = 0

    private val compositeDisposable: CompositeDisposable = CompositeDisposable()

    init {
        imagesWithData.value = emptyArray<ImageWithData>().toMutableList()
    }

    fun requestDataFromBE(offset: Int = lastOffset, query: String = lastQuery) {
        val repository  = GiphyApiRepository()

        compositeDisposable.add(
                repository.getGifs(query, offset)
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeOn(Schedulers.io())
                        .subscribe({
                            result ->
                            var updatedList = emptyArray<ImageWithData>().toMutableList()
                            if (offset != 0) {
                                val values = imagesWithData.value
                                values?.let {
                                    updatedList.addAll(values.toMutableList())
                                }
                            }
                            updatedList.addAll(result.getImagesWithDataFromResponse())
                            imagesWithData.value = updatedList
                        }, {
                            error ->
                            clearImageList()
                            Log.d("gifFetch", "API: gif fetch - failed. Reason: " + error.message)
                        })
        )
    }
    fun getDataToObserve() : MutableLiveData<MutableList<ImageWithData>> {
        return imagesWithData
    }

    fun clearImageList(){
        imagesWithData.postValue(emptyArray<ImageWithData>().toMutableList())
    }

    fun updateQuery(query: String){
        lastQuery = query
    }

    fun increaseOffset() {
        lastOffset += GiphyConstants.DEFAULT_LIMIT_AND_OFFSET
    }

    fun clearOffset(){
        lastOffset = 0
    }
}
