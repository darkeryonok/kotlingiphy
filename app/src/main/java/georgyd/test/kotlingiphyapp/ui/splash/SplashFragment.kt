package georgyd.test.kotlingiphyapp.ui.splash

import android.os.Bundle
import android.os.Handler
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.postDelayed
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import georgyd.test.kotlingiphyapp.R
import georgyd.test.kotlingiphyapp.ui.main.MainActivity
import org.jetbrains.anko.clearTop
import org.jetbrains.anko.intentFor
import org.jetbrains.anko.newTask
import org.jetbrains.anko.singleTop

class SplashFragment : Fragment() {

    companion object {
        fun newInstance() = SplashFragment()
    }

    private lateinit var viewModel: SplashViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View {
        return inflater.inflate(R.layout.splash_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(this).get(SplashViewModel::class.java)
        startAuthorizationWithDelay()
    }

    private fun startAuthorizationWithDelay() {
        Handler().postDelayed(500) {
            context?.let { it.startActivity(it.intentFor<MainActivity>()
                    .newTask()
                    .singleTop()
                    .clearTop()
            ) }
        }
    }

}
