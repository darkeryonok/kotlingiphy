package georgyd.test.kotlingiphyapp.ui.splash

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import georgyd.test.kotlingiphyapp.R

class SplashActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.splash_activity)
        if (savedInstanceState == null) {
            supportFragmentManager.beginTransaction()
                    .replace(R.id.container, SplashFragment.newInstance())
                    .commitNow()
        }
    }

}