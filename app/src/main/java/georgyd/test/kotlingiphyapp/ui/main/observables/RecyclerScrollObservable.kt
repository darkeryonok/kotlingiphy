package georgyd.test.kotlingiphyapp.ui.main.observables

import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import georgyd.test.kotlingiphyapp.ui.main.adapters.GifAdapter
import io.reactivex.Observable
import io.reactivex.subjects.PublishSubject

object RecyclerScrollObservable {
    fun fromData(recyclerView: RecyclerView, adapter: GifAdapter): Observable<Int> {

        val subject = PublishSubject.create<Int>()

        recyclerView.addOnScrollListener(object : RecyclerView.OnScrollListener() {

            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                val layoutManager = recyclerView.layoutManager as LinearLayoutManager
                val lastPosition = layoutManager
                        .findLastVisibleItemPosition()
                if (lastPosition == adapter.itemCount - 1) {
                    subject.onNext(lastPosition)
                }
            }
        })

        return subject
    }

}