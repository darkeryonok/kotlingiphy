package georgyd.test.kotlingiphyapp.data.models

import java.util.ArrayList

data class GiphySearchResponse(
        val data : List<Datum>,
        val pagination : Pagination,
        val meta : Meta
) {
    fun getImagesWithDataFromResponse(): List<ImageWithData> {
        val list = ArrayList<ImageWithData>()
        for (element in data) {
            val image = ImageWithData(
                    element.title,
                    element.id,
                    element.images.original.url
            )
            list.add(image)
        }
        return list
    }

    fun getImageListFromResponse(): List<String> {
        val list = ArrayList<String>()
        for (element in data) {
            list.add(element.images.original.url)
        }
        return list
    }
}