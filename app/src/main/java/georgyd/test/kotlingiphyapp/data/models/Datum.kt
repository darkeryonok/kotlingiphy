package georgyd.test.kotlingiphyapp.data.models

data class Datum(
        val type: String,
        val id: String,
        val slug: String,
        val url: String,
        val bitlyGifUrl: String,
        val bitlyUrl: String,
        val embedUrl: String,
        val username: String,
        val source: String,
        val rating: String,
        val contentUrl: String,
        val sourceTld: String,
        val sourcePostUrl: String,
        val isIndexable: Int,
        val isSticker: Int,
        val importDatetime: String,
        val trendingDatetime: String,
        val images: Images,
        val title: String
)