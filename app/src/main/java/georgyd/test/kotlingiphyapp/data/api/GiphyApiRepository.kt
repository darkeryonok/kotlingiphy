package georgyd.test.kotlingiphyapp.data.api

import android.content.Context
import georgyd.test.kotlingiphyapp.GiphySearchApp
import georgyd.test.kotlingiphyapp.data.models.GiphySearchResponse
import georgyd.test.kotlingiphyapp.utils.GiphyConstants
import io.reactivex.Observable
import java.util.HashMap
import javax.inject.Inject

class GiphyApiRepository {

    @Inject
    lateinit var api: GiphyApiService

    @Inject
    lateinit var context: Context

    init {
        GiphySearchApp.appComponent.inject(this)
    }

    fun getGifs(query : String, offset : Int): Observable<GiphySearchResponse> {

        val options = HashMap<String, String>()
        options[GiphyConstants.PARAMETER_KEY_API_KEY] = GiphyConstants.API_KEY
        options[GiphyConstants.PARAMETER_KEY_LIMIT] = GiphyConstants.DEFAULT_LIMIT_AND_OFFSET.toString()
        options[GiphyConstants.PARAMETER_KEY_OFFSET] = offset.toString()
        options[GiphyConstants.PARAMETER_KEY_QUERY] = query

        return api.getGifsFromQuery(options)
    }

}