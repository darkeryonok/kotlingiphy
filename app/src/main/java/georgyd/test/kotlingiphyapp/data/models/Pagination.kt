package georgyd.test.kotlingiphyapp.data.models

data class Pagination(
        val totalCount: Int,
        val count: Int,
        val offset: Int
)