package georgyd.test.kotlingiphyapp.data.models

data class Meta(
        val status: Int,
        val msg: String,
        val responseId: String
)