package georgyd.test.kotlingiphyapp.data.models

data class Original(
        val url: String,
        val width: String,
        val height: String,
        val size: String,
        val frames: String,
        val mp4: String,
        val mp4Size: String,
        val webp: String,
        val webpSize: String
)