package georgyd.test.kotlingiphyapp.data.models

data class ImageWithData(
        val id: String,
        val title: String,
        val url: String
)