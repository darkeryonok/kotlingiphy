package georgyd.test.kotlingiphyapp.data.api

import android.content.Context
import georgyd.test.kotlingiphyapp.data.models.GiphySearchResponse
import georgyd.test.kotlingiphyapp.utils.GiphyConstants
import io.reactivex.Observable
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.QueryMap
import java.util.concurrent.TimeUnit

interface GiphyApiService {

    @GET("/v1/gifs/search")
    fun getGifsFromQuery(@QueryMap options: Map<String, String>): Observable<GiphySearchResponse>

    companion object Factory {
        fun create(context: Context): GiphyApiService {

            val client = OkHttpClient.Builder().connectTimeout(15, TimeUnit.SECONDS)
                    .build()

            val retrofit = Retrofit.Builder()
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .addConverterFactory(GsonConverterFactory.create())
                    .baseUrl(GiphyConstants.API_GIPHY_URL)
                    .client(client)
                    .build()
            return retrofit.create(GiphyApiService::class.java)
        }
    }

}