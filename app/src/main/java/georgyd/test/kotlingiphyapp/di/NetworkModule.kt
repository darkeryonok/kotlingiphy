package georgyd.test.kotlingiphyapp.di

import android.content.Context
import dagger.Module
import dagger.Provides
import georgyd.test.kotlingiphyapp.data.api.GiphyApiService
import javax.inject.Singleton

@Module
class NetworkModule(private val context: Context) {

    @Provides
    @Singleton
    fun provideApi(): GiphyApiService = GiphyApiService.create(context)

}