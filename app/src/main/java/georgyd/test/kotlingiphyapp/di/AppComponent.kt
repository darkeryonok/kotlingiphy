package georgyd.test.kotlingiphyapp.di

import dagger.Component
import georgyd.test.kotlingiphyapp.data.api.GiphyApiRepository
import georgyd.test.kotlingiphyapp.ui.main.fragments.MainFragment
import javax.inject.Singleton

@Component(modules = arrayOf(AppModule::class, NetworkModule::class))
@Singleton
interface AppComponent {

    fun inject(target: GiphyApiRepository)
    fun inject(target: MainFragment)
}