package georgyd.test.kotlingiphyapp.di

import android.content.Context
import dagger.Module
import dagger.Provides
import georgyd.test.kotlingiphyapp.GiphySearchApp
import javax.inject.Singleton

@Module
class AppModule(private val giphyApp: GiphySearchApp) {

    @Provides
    @Singleton
    fun provideContext(): Context = giphyApp

}